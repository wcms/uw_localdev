#!/bin/sh

## Provision script to install base WCMS machine with reboot
## This file gets executed the first time you do a `vagrant up`, if you want it to
## run again you'll need run `vagrant provision`
echo "Provisioning virtual machine..."

##  skip interactive post install configuration steps
export DEBIAN_FRONTEND=noninteractive

## Set Timezone to America/Toronto from UTC time
echo "America/Toronto" > /etc/timezone

apt-get -y -qq update > /dev/null
apt-get -y -qq upgrade > /dev/null

echo "Installing Docker..."

curl -sSL https://get.docker.com/ | sh

docker run hello-world

gpasswd -a vagrant docker

## Install the dockerfile and build it.

echo "Create Docker file..."

git clone https://github.com/alberto56/docker-drupal.git

echo "Building docker-drupal..."

docker build docker-drupal > /dev/null

echo "Finished building docker-drupal..."

## Install PHPStorm

echo "Getting ready to install PHPStorm..."

## apt-get purge openjdk* 

apt-add-repository ppa:webupd8team/java -y

apt-get update > /dev/null
## So we can install Oracle Java silently we need to accept the license and set that we've seen it
echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | sudo /usr/bin/debconf-set-selections

apt-get install oracle-java8-installer > /dev/null

apt-get install oracle-java8-set-default > /dev/null

echo "Download and install PHPStorm..."

wget -q http://download-cf.jetbrains.com/webide/PhpStorm-9.0.tar.gz

tar -xf PhpStorm-9.0.tar.gz

echo "Finished installing PHPStorm..."
echo "Finished provisioning Virtual Machine..."


