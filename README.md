# WCMS Local Development box

This Vagrant box is meant for local development only. This set up is not hardened and is not secure so
it should only be used as a local development environment.

You need to have Vagrant and Virtualbox installed on your host system. Currently Vagrant 1.7.4 and Virtualbox 5 for Windows. Previous versions may work but we suggest you use the versions listed above (or the Mac equivalent).

A few things to be aware of:
   
1. You can adjust the Private IP (located in the Vagrantfile) to something else if you need to.

2. You can also adjust the name of the box (how it is listed in Virtualbox) to something else in the Vagrantfile if you need to.

3. Once you have the Vagrantfile updated with the correct information, open a terminal in Windows, go to the folder you
   where the Vagrantfile is and type:
   
	vagrant up
   
   That will start the creation of your dev environment, after a few minutes it will complete
   and you can then log in. 
   
   If you don't want your VM any more type:
   
	vagrant destroy
   
   This will destroy your VM and remove it from Virtualbox.
   
4. To login to the guest system the username and password are vagrant.
